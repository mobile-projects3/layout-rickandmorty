import 'package:http/http.dart' as http;

const baseUrl = "https://rickandmortyapi.com/api/character?name=";

class API {
  static Future getMovie(search) async {
    var url = baseUrl + search;
    return await http.get(Uri.parse(url));
  }
}

class Results {
  int id;
  String name;
  String species;
  String image;

  Results(this.id, this.name, this.species, this.image);
}
