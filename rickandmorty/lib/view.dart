import 'package:flutter/material.dart';
import 'dart:convert';
import 'api.dart';

class CaractersListView extends StatefulWidget {
  const CaractersListView({super.key});

  @override
  State<CaractersListView> createState() => _CaractersListViewState();
}

class _CaractersListViewState extends State<CaractersListView> {
  List<Results> movies = List<Results>.empty();
  String search = "Rick";

  _CaractersListViewState() {
    API.getMovie(search).then((response) {
      setState(() {
        final body = json.decode(response.body) as Map<String, dynamic>;
        final results = body["results"] as List<dynamic>;
        movies = results
            .map((e) => Results(e["id"], e["name"], e["species"], e["image"]))
            .toList();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Rick's & Rick's"),
      ),
      body: ListView.builder(
        itemCount: movies.length,
        itemBuilder: (context, index) {
          return ListTile(
            leading: CircleAvatar(
              backgroundImage: NetworkImage(
                movies[index].image,
              ),
            ),
            title: Text(
              movies[index].name,
              style: const TextStyle(
                fontSize: 20.0,
                color: Colors.black,
              ),
            ),
            subtitle: Text(movies[index].species),
            onTap: () {
              AlertDialog(
                title: Text("Success"),
                titleTextStyle: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 20),
                actions: [
                  ElevatedButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text("Close")),
                ],
                content: Text("Saved successfully"),
              );
            },
          );
        },
      ),
    );
  }
}
